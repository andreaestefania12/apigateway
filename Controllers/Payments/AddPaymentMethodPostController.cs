using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Muff.Controllers.Payments
{
    [ApiController]
    [Route("payments")]
    public class AddPaymentMethodPostController : ControllerBase
    {

        [HttpPost("addmethod")]
        public async Task<ActionResult> Execute(Payment request)
        {
            return StatusCode(200, "New Payment Method");
        }
    }

    public class Payment
    {
        public string description { get; set; }
        public bool isCard { get; set; }
        public bool isDefaultMethod { get; set; }

        /*Card model*/
        public string cardNumber { get; set; }
        public string expirationDate { get; set; }
        public string ccv { get; set; }
        public string owner { get; set; }

    }
} 
