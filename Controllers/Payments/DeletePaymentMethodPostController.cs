using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Muff.Controllers.Payments
{
    [ApiController]
    [Route("payments")]
    public class DeletePaymentMethodPostController : ControllerBase
    {

        [HttpPost("deletemethod")]
        public async Task<ActionResult> Execute(DeletePayment request)
        {
            return StatusCode(200, "Delete Payment Method");
        }
    }

    public class DeletePayment
    {
        public string description { get; set; }
        public bool isCard { get; set; }
        public bool isDefaultMethod { get; set; }

        /*Card model*/
        public string cardNumber { get; set; }
        public string expirationDate { get; set; }
        public string ccv { get; set; }
        public string owner { get; set; }

    }
} 
