using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Muff.Controllers.Users
{
    [ApiController]
    [Route("users")]
    public class UpdatePasswordUserPostController : ControllerBase
    {

        [HttpPost("updatepassword")]
        public async Task<ActionResult> Execute(Password request)
        {
            return StatusCode(200, "Update Password");
        }
    }

    public class Password
    {
        public long idCliente { get; set; }
        public string oldpassword { get; set; }
        public string newpassword { get; set; }
    }
} 
