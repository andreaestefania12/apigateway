using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Muff.Controllers.Users
{
    [ApiController]
    [Route("users")]
    public class UserBalanceGetController : ControllerBase
    {

        [HttpGet("balance")]
        public async Task<ActionResult> Execute()
        {
            //Return Balance Model
            return StatusCode(200, "{'description': 'Cancelación de carrera','date': '03/12/2021','price': '-2000'  }");
        }
    }
} 
