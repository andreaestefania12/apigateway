using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Muff.Controllers.Users
{
    [ApiController]
    [Route("users")]
    public class LoginUserPostController : ControllerBase
    {

        [HttpPost("login")]
        public async Task<ActionResult> Execute(Login request)
        {
            return StatusCode(200, "Login");
        }        
    }
    public class Login
    {
        public string password { get; set; }
        public string cellphonenumber { get; set; }
    }
    
} 
