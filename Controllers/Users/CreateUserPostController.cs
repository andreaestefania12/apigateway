using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Muff.Controllers.Users
{
    [ApiController]
    [Route("users")]
    public class CreateUserPostController : ControllerBase
    {

        [HttpPost("create")]
        public async Task<ActionResult> Execute(Request request)
        {
            return StatusCode(200, "Ok");
        }
    }

    public class Request
    {
        public string name { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string cellphonenumber { get; set; }
        public string identification { get; set; }
        public string documentType { get; set; }
        public string birthdate { get; set; }
    }
} 
