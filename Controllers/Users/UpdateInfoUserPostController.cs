using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Muff.Controllers.Users
{
    [ApiController]
    [Route("users")]
    public class UpdateInfoUserPostController : ControllerBase
    {

        [HttpPost("updateinfo")]
        public async Task<ActionResult> Execute(Information request)
        {
            return StatusCode(200, "Update Info");
        }
    }

    public class Information
    {
        public long idCliente { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
    }
} 
