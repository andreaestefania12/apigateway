using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Muff.Controllers.Users
{
    [ApiController]
    [Route("users")]
    public class DocumentTypesUserGetController : ControllerBase
    {

        [HttpGet("documenttype")]
        public async Task<ActionResult> Execute()
        {
            return StatusCode(200, "{'id': '1','tipo': cédula de ciudadania}");
            // Return User Model
        }
    }
} 
