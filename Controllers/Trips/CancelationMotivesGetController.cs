using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Muff.Controllers.Trips
{
    [ApiController]
    [Route("trips")]
    public class CancelationMotivesGetController : ControllerBase
    {

        [HttpGet("cancelationmotives")]
        public async Task<ActionResult> Execute()
        {
            //Return Cancelation Model
            return StatusCode(200, "{'id': 1,'description':'El conductor se tardó demasiado'}");
        }
    }
} 
