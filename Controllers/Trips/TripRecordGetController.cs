using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Muff.Controllers.Trips
{
    [ApiController]
    [Route("trips")]
    public class TripRecordGetController : ControllerBase
    {

        [HttpGet("record")]
        public async Task<ActionResult> Execute()
        {
            //Return Trip Model
            return StatusCode(200, "{'idtrip': 1,'realstartlatitude': 3.3,        'realstartlongitude': -76.88,        'realfinallatitude': 3.4,        'realfinallongitude': -76.56,        'startaddress': 'Carrera 39 #7-19',        'finaladdress': 'Calle 5 #7-03',        'servicecost': 15000,        'startdate': 'Diciembre 26 de 2020',        'drivername': 'Juan',        'vehiclemodel': '2015',        'vehiclebrand': 'Jeep',        'IdClient': 1");
        }
    }
} 
